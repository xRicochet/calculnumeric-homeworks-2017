//import Jama.Matrix;

import Jama.SingularValueDecomposition;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Main {
    private static int lines;      // p
    private static int columns;    // n
    private static final double EPSILON = Math.pow(10, -15);
    private static Random random = new Random();
    private static Double existingValue = 0.;
    private static Boolean CHECK = true;

    public static void main(String[] args) throws IOException {
//        int matrixDimension = random.nextInt(1000 - 500 + 1) + 500;
//        Matrix primulPunct = generateRandomMatrix(matrixDimension);
//        Matrix x = citireMatriceRara("");
//        afisareElementeMatriceRara(x);

        ultimulPunct();
//        afisareElementeMatriceRara(primulPunct);
    }

    private static void ultimulPunct() {

        int p = 7, n = 6;
        Jama.Matrix B = Jama.Matrix.random(6, 4);
        Jama.Matrix A = Jama.Matrix.random(p, n).times(B).times(B.transpose());
        System.out.print("A = ");
        A.print(8, 7);
        System.out.println();

        SingularValueDecomposition s = A.svd();

        System.out.print("U = ");
        Jama.Matrix U = s.getU();
        U.print(8, 7);

        System.out.print("Sigma = ");
        Jama.Matrix S = s.getS();
        S.print(8, 7);

        System.out.print("V = ");
        Jama.Matrix V = s.getV();
        V.print(8, 7);

        System.out.print("Valorile singulare ale matricii A: ");
        Jama.Matrix svalues = new Jama.Matrix(s.getSingularValues(), 1);
        svalues.print(8, 7);

        System.out.println("Rangul matricii A: " + s.rank());
        System.out.println("Numarul de conditionare al matricii A: " + s.cond());

        Jama.Matrix USVt = (U.times(S)).times(V.transpose());
        System.out.println("Norma ||A-USV(t)||inf: " + (A.minus(USVt)).normInf());
        System.out.println("");
    }

    //TODO: verificat ca sunt <10 elemente pe linie
    //TODO: verificat cazul cand exista deja element pe line,randomColumn
    private static Matrix generateRandomMatrix(int matrixDimension) {
        Matrix generatedMaxtrix = new Matrix(matrixDimension);
        for (int line = 0; line < matrixDimension; line++) {
//            System.out.println("line: " + line);
            int nrValuesOnLine = random.nextInt(10);    //between 0 and 10
//            System.out.println("nrValuesOnLine: " + nrValuesOnLine);
            if (!checkSizeLine(generatedMaxtrix, line))
                continue;
            int tries = 0;
            for (int i = 0; i < nrValuesOnLine; i++) {
                if (tries > 10)
                    continue;
                int randomColumn = random.nextInt(matrixDimension - line); // between 0 and columns
//                System.out.println("randomColumn: " + randomColumn);
                if (randomColumn != line) {
                    double value = random.nextDouble();
//                    System.out.println("value: " + value);
                    getLineColumnValue(line, randomColumn, generatedMaxtrix);
                    if (existingValue == 0.) {
                        generatedMaxtrix.getVectorValCol().get(line).add(new ValoareColoana(value, randomColumn));
                        System.out.println("\nInserted (" + line + ")(" + randomColumn + ") : " + value);

                        generatedMaxtrix.getVectorValCol().get(randomColumn).add(new ValoareColoana(value, line));
                        System.out.println("Inserted (" + randomColumn + ")(" + line + ") : " + value);
                    } else {
                        i--;
                        tries++;
                    }
                    /*getLineColumnValue(randomColumn, line, generatedMaxtrix);
                    if (existingValue == 0.) {
                        generatedMaxtrix.getVectorValCol().get(randomColumn).add(new ValoareColoana(value, line));
                        System.out.println("Inserted (" + randomColumn + ")(" + line + ") : " + value);

                    } else {
                        i--;
                    }*/
                } else {
                    i--;
                }
            }
        }
        return generatedMaxtrix;
    }

    private static void getLineColumnValue(Integer line, Integer column, Matrix matrix) {
        existingValue = 0.;
        matrix.getVectorValCol().get(line)
                .forEach(VC ->
                {
                    if (Objects.equals(column, VC.getColoana())) {
                        existingValue = VC.getValoare();
                    }
                });
    }

    private static Boolean checkSizeLine(Matrix matrix, Integer line) {
        return matrix.getVectorValCol().get(line).size() < 9;
    }

    private static void afisareElementeMatriceRara(Matrix matrix) {
        matrix.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        {
                            System.out.print("Line: " + line + "-> \t");
                            arrayValueColumn.forEach(VC -> System.out.print("Column " + VC.getColoana() + ": Value " + VC.getValoare() + "||\t"));
                            System.out.print("\n");
                        }
                );
    }

    private static Matrix citireMatriceRara(final String filePath) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\rico\\Desktop\\CalculNumeric6\\src\\main\\resources\\m_rar_sim_2017.txt"));
        Matrix matrix = new Matrix(Integer.valueOf(bufferedReader.readLine()));

//        bufferedReader.readLine();  //empty line

        /*for (int index = 0; index < matrix.getDimensiuneMatrice(); index++) {
            matrix.getVectorB().set(index, Double.valueOf(bufferedReader.readLine()));
        }*/
        bufferedReader.readLine();  //empty line
        String textLine;
        while ((textLine = bufferedReader.readLine()) != null) {
            List<String> textArguments = Arrays.asList(textLine.split("[, ]+"));
            final Double value = Double.valueOf(textArguments.get(0));
            final Integer line = Integer.parseInt(textArguments.get(1));
            final Integer column = Integer.parseInt(textArguments.get(2));

            if (Objects.equals(line, column))  //element pe diagonala principala
            {
                matrix.getVectorDiagonala().set(line, value);
            } else {
//                 tre sa verific daca nu exista pe Line si coloana deja o valoare
                matrix.getVectorValCol().get(line).forEach((ValoareColoana VC) -> {
                    if (Objects.equals(VC.getColoana(), column)) {  //daca mai exista un element pe aceeasi linie si coloana il adunam
                        System.out.println("Element duplicat pe linia " + line + " si coloana " + column);
                        VC.setValoare(VC.getValoare() + value);     //adun valoarea actuala la cea precedenta
                    }
                });
                matrix.getVectorValCol().get(line).add(new ValoareColoana(value, column));
            }
        }
        bufferedReader.close();
        return matrix;
    }
}