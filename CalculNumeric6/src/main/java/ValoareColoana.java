/**
 * Created by a.olteanu on 3/28/2017.
 */
public class ValoareColoana {
    private Double valoare;
    private Integer coloana;

    public ValoareColoana(Double valoare, Integer coloana) {
        this.valoare = valoare;
        this.coloana = coloana;
    }

    public Double getValoare() {
        return valoare;
    }

    public void setValoare(Double valoare) {
        this.valoare = valoare;
    }

    public Integer getColoana() {
        return coloana;
    }

    public void setColoana(Integer coloana) {
        this.coloana = coloana;
    }
}
