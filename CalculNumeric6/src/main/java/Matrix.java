import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Matrix {
    private Integer dimensiuneMatrice;
    private List<Double> vectorB;
    private List<Double> vectorDiagonala;
    private Map<Integer, List<ValoareColoana>> vectorValCol;

    public Matrix(Integer dimensiuneMatrice) {
        this.dimensiuneMatrice = dimensiuneMatrice;
        vectorB = new ArrayList<>(this.dimensiuneMatrice);
        vectorDiagonala = new ArrayList<>(this.dimensiuneMatrice);
        vectorValCol = new HashMap<>();

        for (int index = 0; index < dimensiuneMatrice; index++) {
            vectorB.add(0.);
            vectorDiagonala.add(0.);
            vectorValCol.put(index, new ArrayList<>());
        }

    }

    public Integer getDimensiuneMatrice() {
        return dimensiuneMatrice;
    }

    public void setDimensiuneMatrice(Integer dimensiuneMatrice) {
        this.dimensiuneMatrice = dimensiuneMatrice;
    }

    public List<Double> getVectorB() {
        return vectorB;
    }

    public void setVectorB(List<Double> vectorB) {
        this.vectorB = vectorB;
    }

    public List<Double> getVectorDiagonala() {
        return vectorDiagonala;
    }

    public void setVectorDiagonala(List<Double> vectorDiagonala) {
        this.vectorDiagonala = vectorDiagonala;
    }

    public Map<Integer, List<ValoareColoana>> getVectorValCol() {
        return vectorValCol;
    }

    public void setVectorValCol(Map<Integer, List<ValoareColoana>> vectorValCol) {
        this.vectorValCol = vectorValCol;
    }
}
