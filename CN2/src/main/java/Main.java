import Jama.LUDecomposition;
import Jama.Matrix;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Stuff.epsilon = Math.pow(10,-10);
        //citere matrice din fisier
        Matrix matrix = new Matrix(Stuff.matriceaA1);
        System.out.println("A:");
        matrix.print(4, 1);

        System.out.println("\n--------------------------a)--------------------------");
        descompunereCholeski(Stuff.matriceaA1);

        System.out.println("\n--------------------------b)--------------------------");
        System.out.println("det(A) = det(L)*det(D)*det(L^T) = det(D) = " + computeDeterminant());

        System.out.println("\n--------------------------c)--------------------------");
        findXcholeski(Stuff.matriceaA1, Stuff.vectorulB);

        System.out.println("\n--------------------------d)--------------------------");
        jamaUsage();

    }

    private static void jamaUsage() {
        Matrix matrix = new Matrix(Stuff.matriceaA1);
        LUDecomposition lu = matrix.lu();
        System.out.println("L:");
        lu.getL().print(6,3);
        System.out.println("D:");
        lu.getU().print(6,3);

        Matrix A = new Matrix(Stuff.matriceaA1);
        double[][] matriceaB = new double[Stuff.matriceaA1[0].length][1];
        for(int index = 0 ; index < Stuff.matriceaA1[0].length ; index++){
            matriceaB[index][0] = Stuff.vectorulB[index];
        }
        Matrix b = new Matrix(matriceaB);
        Matrix x = A.solve(b);
        System.out.println("X:");
        x.print(6,3);
        Matrix r = A.times(x).minus(b);
        System.out.println("\n--------------------------e)--------------------------");
        System.out.println("Ax-b");
        r.print(6,50);
        double rnorm = r.normInf();
        System.out.println("Norma:\t" + rnorm);
    }

    private static boolean verificareImpartire(double impartitor){
        return Math.abs(impartitor) > Stuff.epsilon;
    }

    // A = L D L^T
    private static void findXcholeski(double[][] matriceaA1, double[] vectorulB) {
        int dimensiuneaMatricii = matriceaA1[0].length;

        //rezolvare L * z = b
        double z[][] = new double[dimensiuneaMatricii][1];
        //rezolvare D * y=z
        double y[][] = new double[dimensiuneaMatricii][1];
        //rezolvare L^T * x = y
        double greatX[][] = new double[dimensiuneaMatricii][1];

        for(int index=0; index < dimensiuneaMatricii ; index++) {
            double suma = 0;
            for (int j = 0; j <= index - 1; j++) {
                suma += Stuff.matriceaL[index][j] * z[j][0];
            }
            z[index][0] = vectorulB[index] - suma;

            if(verificareImpartire(Stuff.vectorulD[index]))
                y[index][0] = z[index][0] / Stuff.vectorulD[index];
            else
                throw new RuntimeException("nu se poate face impartirea.");
        }
        System.out.println("Z\t" + Arrays.deepToString(z));
        System.out.println("Y\t" + Arrays.deepToString(y));

        for(int index=dimensiuneaMatricii - 1; index >=0 ; index--) {
            double suma = 0;
            for (int j = index; j <dimensiuneaMatricii; j++) {
                suma += Stuff.matriceaL[j][index] * greatX[j][0];
            }
            greatX[index][0] = y[index][0] - suma;
        }
        Stuff.matriceaX = greatX.clone();
        System.out.println("X\t" + Arrays.deepToString(Stuff.matriceaX));

    }

    private static double computeDeterminant() {
        double determinantD = 1;
        for (int index = 0; index < Stuff.vectorulD.length; index++)
            determinantD *= Stuff.vectorulD[index];

        return determinantD;
    }

    private static void descompunereCholeski(double[][] matriceaA) {
        int dimensiuneaMatricii = matriceaA[0].length;
        double matriceaL[][] = new double[dimensiuneaMatricii][dimensiuneaMatricii];
        for (int i = 0; i < dimensiuneaMatricii; i++) {
            matriceaL[i][i] = 1;
        }
        double vectorulD[] = new double[dimensiuneaMatricii];

//        pasul p : [0,n-1]
        for (int pasulP = 0; pasulP < dimensiuneaMatricii; pasulP++) {
            double sumaD = 0;
            for (int k = 0; k <= pasulP - 1; k++) {
                sumaD += vectorulD[k] * Math.pow(matriceaL[pasulP][k], 2);
            }
            vectorulD[pasulP] = matriceaA[pasulP][pasulP] - sumaD;

            for (int i = pasulP + 1; i < dimensiuneaMatricii; i++) {
                double sumaL = 0;
                for (int k = 0; k <= pasulP - 1; k++) {
                    sumaL += vectorulD[k] * matriceaL[i][k] * matriceaL[pasulP][k];
                }
                if(verificareImpartire(vectorulD[pasulP]))
                    matriceaL[i][pasulP] = (matriceaA[i][pasulP] - sumaL) / vectorulD[pasulP];
                else
                    throw new RuntimeException("nu se poate face impartirea.");
            }

            Stuff.matriceaL = matriceaL.clone();
            Stuff.vectorulD = vectorulD.clone();
        }

        System.out.println("D\t" + Arrays.toString(Stuff.vectorulD));
        System.out.println("L\t" + Arrays.deepToString(Stuff.matriceaL));

    }


}
