import com.sun.xml.internal.ws.transport.http.ResourceLoader;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class Main {
    private static final double EPSILON = Math.pow(10, -15);
    private static boolean CHECK = false;
    private static Double existingValue;

    public static void main(String[] args) {
        Matrix matrice = null;
        try {
            matrice = citireMatriceRara("/m_rar_2017_4.txt");
        } catch (Exception e) {
            System.out.println("Una dintre matrici nu a putut fi citita....sorry");
            e.printStackTrace();
            System.exit(-1);
        }
        verificareMatrice(matrice);

        List<Double> x = findNemo(matrice);

        if (x != null) {
            calculNormaInfinit(matrice, x);
            List<Double> xA = inmultireMatriceVector(x, matrice);
//            compareVectors(xA, matrice.getVectorB());

            x.forEach(element -> System.out.print(element + "; "));
        } else
            System.out.println("Divergenta");

    }

    private static void calculNormaInfinit(Matrix matrice, List<Double> x) {
        Double maxNorm = 0.;
        List<Double> xA = inmultireMatriceVector(x, matrice);
        for (int index = 0; index < matrice.getDimensiuneMatrice(); index++) {
            Double v = Math.abs(xA.get(index) - matrice.getVectorB().get(index));
            maxNorm = (maxNorm < v) ? v : maxNorm;
        }
        System.out.println("MAX NORM: " + maxNorm);
    }

    private static List<Double> findNemo(Matrix matrice) {
        int dimensiuneVector = matrice.getDimensiuneMatrice();
        int k = 0;
        Double euclidianNorm;
        List<Double> nemo = new ArrayList<>(dimensiuneVector);

        for (int index = 0; index < dimensiuneVector; index++) {
//            nemo.add(index, (double) (index + 1));
            nemo.add(index, 0.);
//            nemo.add(index, (double) index);
        }
        long startTime = System.currentTimeMillis();

        do {
            euclidianNorm = 0.;
            for (int index = 0; index < dimensiuneVector; index++) {
                Double xK_1 = nemo.get(index);
                Double xI, sum = 0.;
                xI = matrice.getVectorB().get(index);

                for (int j = 0; j < index - 1; j++) {
                    getLineColumnValue(index, j, matrice);
                    sum += existingValue * nemo.get(j);
                }

                xI -= sum;
                sum = 0.;

                for (int j = index + 1; j < dimensiuneVector; j++) {
                    getLineColumnValue(index, j, matrice);
                    sum += existingValue * nemo.get(j);
                }

                xI -= sum;
                nemo.set(index, xI / matrice.getVectorDiagonala().get(index));
                euclidianNorm += Math.pow(nemo.get(index) - xK_1, 2);
            }
            k++;
            euclidianNorm = Math.sqrt(euclidianNorm);
            System.out.println(k + "\t\t\t" + new BigDecimal(euclidianNorm, new MathContext(150)).toString());

        } while (Math.abs(euclidianNorm) >= EPSILON && k < 10000 && euclidianNorm < Math.pow(10, 8));

        long endTime = System.currentTimeMillis();
        System.out.println("That took " + (endTime - startTime) / 1000 + " seconds");

        if (Math.abs(euclidianNorm) < EPSILON) {
            System.out.println("Numarul de interatii necesare:\t" + k);
            return nemo;
        }

        return null;
    }

    private static void compareVectors(List<Double> vectorA, List<Double> vectorB) {
        CHECK = true;
        for (int index = 0; index < vectorA.size(); index++) {
            if (Math.abs(vectorA.get(index) - vectorB.get(index)) > EPSILON) {
                System.out.println("A" + index + ": " + vectorA.get(index));
                System.out.println("B" + index + ": " + vectorB.get(index));
                CHECK = false;
            }
        }

        if (CHECK)
            System.out.println("Vectorii sunt egali");
        else
            System.out.println("Vectorii nu sunt egali");
    }

    private static List<Double> inmultireMatriceVector(List<Double> x, Matrix matriceaA) {
        List<Double> inmultireMatriceVector = new ArrayList<>(matriceaA.getDimensiuneMatrice());
        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            inmultireMatriceVector.add(index, 0.);
        }

        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(VC -> {
                            Double sumaTemporara = inmultireMatriceVector.get(line);
                            Double termen = x.get(VC.getColoana()) * VC.getValoare();
                            inmultireMatriceVector.set(line, (sumaTemporara + termen));
                        }));
        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            Double sumaTemporara = inmultireMatriceVector.get(index);
            Double termen = x.get(index) * matriceaA.getVectorDiagonala().get(index);
            inmultireMatriceVector.set(index, (sumaTemporara + termen));
        }
        return inmultireMatriceVector;
    }

    private static Matrix inmultireMatrici(Matrix matriceaA, Matrix matriceaB) {
        Matrix inmultireMatrice = new Matrix(matriceaA.getDimensiuneMatrice());
        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(valueColumnA ->
                        {
    //                        Double sumaTemporara = inmultireMatrice.getVectorDiagonala().get()
                        }));

        return new Matrix(matriceaA.getDimensiuneMatrice());
    }

    private static void compareMatrixes(Matrix matriceaA, Matrix matriceaB, String name, String fileName) {
        CHECK = true;

        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(valueColumnA ->
                        {
                            matriceaB.getVectorValCol().get(line)
                                    .forEach(valueColumnB ->
                                    {
                                        if (Objects.equals(valueColumnA.getColoana(), valueColumnB.getColoana()))
                                            if (!Objects.equals(valueColumnA.getValoare(), valueColumnB.getValoare()))
                                                CHECK = false;
                                    });
                        }));

        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            if (!Objects.equals(matriceaA.getVectorDiagonala().get(index), matriceaB.getVectorDiagonala().get(index)))
                CHECK = false;
        }

        if (CHECK)
            System.out.println("A" + name + "B este egala cu matricea din a" + fileName + "b.txt");
        else
            System.out.println("A" + name + "B nu este egala cu matricea din a" + fileName + "b.txt");
    }

    private static Matrix sumaMatrice(Matrix matriceaA, Matrix matriceaB) {
        Matrix sumaMatrix = new Matrix(matriceaA.getDimensiuneMatrice());
        for (int index = 0; index < matriceaA.getVectorDiagonala().size(); index++) {
            //suma elementelor de pe diagonala principala
            Double elementDiagonalaA = matriceaA.getVectorDiagonala().get(index);
            Double elementDiagonalaB = matriceaB.getVectorDiagonala().get(index);
            sumaMatrix.getVectorDiagonala().set(index, (elementDiagonalaA + elementDiagonalaB));

            //suma elementelor din vectorul B al ambelor matrici
            Double elementVectorBA = matriceaA.getVectorB().get(index);
            Double elementVectorBB = matriceaB.getVectorB().get(index);
            sumaMatrix.getVectorB().set(index, (elementVectorBA + elementVectorBB));
        }

        //iterez prin matriceaA, daca gasesc element pe aceeasi pozitie in B il adun, daca nu adaug doar valoarea din A
        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->    //fiecare linie
                        arrayValueColumn.forEach(VC -> {    //fiecare element nenul de pe linie

                            getLineColumnValue(line, VC.getColoana(), matriceaB);

                            sumaMatrix.getVectorValCol()
                                    .get(line)
                                    .add(new ValoareColoana((VC.getValoare() + existingValue), VC.getColoana()));

                        }));
        //iterez prin matriceaB, daca nu gasesc element in sumaMatrix inseamna ca nu exista un element in A si doar adaug elementul din B
        matriceaB.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(VC ->
                        {
                            getLineColumnValue(line, VC.getColoana(), sumaMatrix);
                            if (existingValue == 0.) {
                                sumaMatrix.getVectorValCol()
                                        .get(line)
                                        .add(new ValoareColoana((VC.getValoare()), VC.getColoana()));
                            }
                        }));
        return sumaMatrix;
    }

    /**
     * Salveaza valoarea din matrix de pe pozitia line,column
     *
     * @param line   linia din matrix
     * @param column coloana din matrix
     * @param matrix matricea pe care se face cautarea
     */
    private static void getLineColumnValue(Integer line, Integer column, Matrix matrix) {
        existingValue = 0.;
        matrix.getVectorValCol().get(line)
                .forEach(VC ->
                {
                    if (Objects.equals(column, VC.getColoana())) {
                        existingValue = VC.getValoare();
                    }
                });
    }

    private static Matrix citireMatriceRara(final String filePath) throws IOException {

        InputStream resourceAsStream = ResourceLoader.class.getResourceAsStream(filePath);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream, "UTF-8"));
        Matrix matrix = new Matrix(Integer.valueOf(bufferedReader.readLine()));

        bufferedReader.readLine();  //empty line

        for (int index = 0; index < matrix.getDimensiuneMatrice(); index++) {
            matrix.getVectorB().set(index, Double.valueOf(bufferedReader.readLine()));
        }
        bufferedReader.readLine();  //empty line
        String textLine;
        while ((textLine = bufferedReader.readLine()) != null) {
            List<String> textArguments = Arrays.asList(textLine.split("[, ]+"));
            final Double value = Double.valueOf(textArguments.get(0));
            final Integer line = Integer.parseInt(textArguments.get(1));
            final Integer column = Integer.parseInt(textArguments.get(2));

            if (Objects.equals(line, column))  //element pe diagonala principala
            {
                matrix.getVectorDiagonala().set(line, value);
            } else {
//                 tre sa verific daca nu exista pe Line si coloana deja o valoare
                matrix.getVectorValCol().get(line).forEach((ValoareColoana VC) -> {
                    if (Objects.equals(VC.getColoana(), column)) {  //daca mai exista un element pe aceeasi linie si coloana il adunam
                        System.out.println("Element duplicat pe linia " + line + " si coloana " + column);
                        VC.setValoare(VC.getValoare() + value);     //adun valoarea actuala la cea precedenta
                    }
                });
                matrix.getVectorValCol().get(line).add(new ValoareColoana(value, column));
            }
        }

//        afisareElementeMatriceRara(matrix);
//        verificareMatrice(matrix);
        bufferedReader.close();
        return matrix;
    }

    private static void verificareMatrice(Matrix matrix) {
        CHECK = false;
        matrix.getVectorValCol().forEach((k, v) ->
        {
            //matricea trebuie sa aiba cel mult 10 elemente nenule pe fiecare linie
            //presupun ca elementul de pe diagonala principala e mereu nenul
            if (v.size() > 9) {
                System.out.println("Matricea are " + (1 + v.size()) + " valori pe linia " + k + "\nProgramul se va opri");
                System.exit(-1);
            }
        });
        matrix.getVectorDiagonala().forEach(elementDiagonala ->
        {
            if (Math.abs(elementDiagonala) < EPSILON) {
                System.out.println("Matricea contine element nul pe diagonala principala.\nProgramul se va opri.");
                System.exit(-1);
            }
        });
    }

    private static void afisareElementeMatriceRara(Matrix matrix) {
        matrix.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        {
                            System.out.print("Line: " + line + "-> \t");
                            arrayValueColumn.forEach(VC -> System.out.print("Column " + VC.getColoana() + ": Value " + VC.getValoare() + "||\t"));
                            System.out.print("\n");
                        }
                );
    }
}
