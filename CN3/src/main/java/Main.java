import com.sun.xml.internal.ws.transport.http.ResourceLoader;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Main {
    private static boolean check = false;
    private static Double existingValue;

    public static void main(String[] args) {
        Matrix matriceaA = null;
        Matrix matriceaB = null;
        Matrix matriceaAPlusB = null;
        Matrix matriceaAPlusBCalculata;
        Matrix matriceaAOriB = null;
        Matrix matriceaAOriBCalculata;
        List<Double> vectorAOriX;

        try {
            matriceaA = citireMatriceRara("/a.txt");
            verificareMatrice(matriceaA);
            matriceaB = citireMatriceRara("/b.txt");
            verificareMatrice(matriceaB);
            matriceaAPlusB = citireMatriceRara("/aplusb.txt");
            matriceaAOriB = citireMatriceRara("/aorib.txt");

        } catch (Exception e) {
            System.out.println("Una dintre matrici nu a putut fi citita....sorry");
            e.printStackTrace();
        }

//        afisareElementeMatriceRara(matriceaA);

        matriceaAPlusBCalculata = sumaMatrice(matriceaA, matriceaB);
        compareMatrixes(matriceaAPlusB, matriceaAPlusBCalculata, "+", "plus");

        matriceaAOriBCalculata = inmultireMatrici(matriceaA, matriceaB);
        compareMatrixes(matriceaAOriB, matriceaAOriBCalculata, "*", "ori");

        vectorAOriX = inmultireMatriceVector(matriceaA);

        assert matriceaA != null;
        compareVectors(vectorAOriX, matriceaA.getVectorB());


    }

    private static void compareVectors(List<Double> vectorA, List<Double> vectorB) {
        check = true;
        for (int index = 0; index < vectorA.size(); index++) {
            if (!Objects.equals(vectorA.get(index), vectorB.get(index))) {
                System.out.println("A" + index + ": " + vectorA.get(index));
                System.out.println("B" + index + ": " + vectorB.get(index));

                check = false;
            }
        }

        if (check)
            System.out.println("Vectorii sunt egali");
        else
            System.out.println("Vectorii nu sunt egali");
    }

    private static List<Double> inmultireMatriceVector(Matrix matriceaA) {
        List<Double> x = new ArrayList<>(matriceaA.getDimensiuneMatrice());
        List<Double> inmultireMatriceVector = new ArrayList<>(matriceaA.getDimensiuneMatrice());
        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            x.add(index, (double) (matriceaA.getDimensiuneMatrice() - index));
            inmultireMatriceVector.add(index, 0.);
        }

        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(VC -> {
                            Double sumaTemporara = inmultireMatriceVector.get(line);
                            Double termen = x.get(VC.getColoana()) * VC.getValoare();
                            inmultireMatriceVector.set(line, (sumaTemporara + termen));
                        }));
        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            Double sumaTemporara = inmultireMatriceVector.get(index);
            Double termen = x.get(index) * matriceaA.getVectorDiagonala().get(index);
            inmultireMatriceVector.set(index, (sumaTemporara + termen));
        }
        return inmultireMatriceVector;
    }

    private static Matrix inmultireMatrici(Matrix matriceaA, Matrix matriceaB) {
        Matrix inmultireMatrice = new Matrix(matriceaA.getDimensiuneMatrice());
        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(valueColumnA ->
                        {
    //                        Double sumaTemporara = inmultireMatrice.getVectorDiagonala().get()
                        }));

        return new Matrix(matriceaA.getDimensiuneMatrice());
    }

    private static void compareMatrixes(Matrix matriceaA, Matrix matriceaB, String name, String fileName) {
        check = true;

        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(valueColumnA ->
                        {
                            matriceaB.getVectorValCol().get(line)
                                    .forEach(valueColumnB ->
                                    {
                                        if (Objects.equals(valueColumnA.getColoana(), valueColumnB.getColoana()))
                                            if (!Objects.equals(valueColumnA.getValoare(), valueColumnB.getValoare()))
                                                check = false;
                                    });
                        }));

        for (int index = 0; index < matriceaA.getDimensiuneMatrice(); index++) {
            if (!Objects.equals(matriceaA.getVectorDiagonala().get(index), matriceaB.getVectorDiagonala().get(index)))
                check = false;
        }

        if (check)
            System.out.println("A" + name + "B este egala cu matricea din a" + fileName + "b.txt");
        else
            System.out.println("A" + name + "B nu este egala cu matricea din a" + fileName + "b.txt");
    }

    private static Matrix sumaMatrice(Matrix matriceaA, Matrix matriceaB) {
        Matrix sumaMatrix = new Matrix(matriceaA.getDimensiuneMatrice());
        for (int index = 0; index < matriceaA.getVectorDiagonala().size(); index++) {
            //suma elementelor de pe diagonala principala
            Double elementDiagonalaA = matriceaA.getVectorDiagonala().get(index);
            Double elementDiagonalaB = matriceaB.getVectorDiagonala().get(index);
            sumaMatrix.getVectorDiagonala().set(index, (elementDiagonalaA + elementDiagonalaB));

            //suma elementelor din vectorul B al ambelor matrici
            Double elementVectorBA = matriceaA.getVectorB().get(index);
            Double elementVectorBB = matriceaB.getVectorB().get(index);
            sumaMatrix.getVectorB().set(index, (elementVectorBA + elementVectorBB));
        }

        //iterez prin matriceaA, daca gasesc element pe aceeasi pozitie in B il adun, daca nu adaug doar valoarea din A
        matriceaA.getVectorValCol()
                .forEach((line, arrayValueColumn) ->    //fiecare linie
                        arrayValueColumn.forEach(VC -> {    //fiecare element nenul de pe linie

                            getLineColumnValue(line, VC.getColoana(), matriceaB);

                            sumaMatrix.getVectorValCol()
                                    .get(line)
                                    .add(new ValoareColoana((VC.getValoare() + existingValue), VC.getColoana()));

                        }));
        //iterez prin matriceaB, daca nu gasesc element in sumaMatrix inseamna ca nu exista un element in A si doar adaug elementul din B
        matriceaB.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        arrayValueColumn.forEach(VC ->
                        {
                            getLineColumnValue(line, VC.getColoana(), sumaMatrix);
                            if (existingValue == 0.) {
                                sumaMatrix.getVectorValCol()
                                        .get(line)
                                        .add(new ValoareColoana((VC.getValoare()), VC.getColoana()));
                            }
                        }));
        return sumaMatrix;
    }

    /**
     * Salveaza valoarea din matrix de pe pozitia line,column
     *
     * @param line   linia din matrix
     * @param column coloana din matrix
     * @param matrix matricea pe care se face cautarea
     */
    private static void getLineColumnValue(Integer line, Integer column, Matrix matrix) {
        existingValue = 0.;
        matrix.getVectorValCol().get(line)
                .forEach(VC ->
                {
                    if (Objects.equals(column, VC.getColoana())) {
                        existingValue = VC.getValoare();
                    }
                });
    }

    private static Matrix citireMatriceRara(final String filePath) throws IOException {

        InputStream resourceAsStream = ResourceLoader.class.getResourceAsStream(filePath);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream,"UTF-8"));
       Matrix matrix = new Matrix(Integer.valueOf(bufferedReader.readLine()));

        bufferedReader.readLine();  //empty line

        for (int index = 0; index < matrix.getDimensiuneMatrice(); index++) {
            matrix.getVectorB().set(index, Double.valueOf(bufferedReader.readLine()));
        }
        bufferedReader.readLine();  //empty line
        String textLine;
        while ((textLine = bufferedReader.readLine()) != null) {
            List<String> textArguments = Arrays.asList(textLine.split("[, ]+"));
            final Double value = Double.valueOf(textArguments.get(0));
            final Integer line = Integer.parseInt(textArguments.get(1));
            final Integer column = Integer.parseInt(textArguments.get(2));

            if (Objects.equals(line, column))  //element pe diagonala principala
            {
                matrix.getVectorDiagonala().set(line, value);
            } else {
//                 tre sa verific daca nu exista pe Line si coloana deja o valoare
                matrix.getVectorValCol().get(line).forEach((ValoareColoana VC) -> {
                    if (Objects.equals(VC.getColoana(), column)) {  //daca mai exista un element pe aceeasi linie si coloana il adunam
                        System.out.println("Element duplicat pe linia " + line + " si coloana " + column);
                        VC.setValoare(VC.getValoare() + value);     //adun valoarea actuala la cea precedenta
                    }
                });
                matrix.getVectorValCol().get(line).add(new ValoareColoana(value, column));
            }
        }

//        afisareElementeMatriceRara(matrix);
//        verificareMatrice(matrix);
        bufferedReader.close();
        return matrix;
    }

    private static void verificareMatrice(Matrix matrix) {
        check = false;
        matrix.getVectorValCol().forEach((k, v) ->
        {
            //matricea trebuie sa aiba cel mult 10 elemente nenule pe fiecare linie
            //presupun ca elementul de pe diagonala principala e mereu nenul
            if (v.size() > 9) {
                System.out.println("Matricea are " + (1 + v.size()) + " valori pe linia " + k + "\nProgramul se va opri");
                System.exit(2);
            }
        });
    }

    private static void afisareElementeMatriceRara(Matrix matrix) {
        matrix.getVectorValCol()
                .forEach((line, arrayValueColumn) ->
                        {
                            System.out.print("Line: " + line + "-> \t");
                            arrayValueColumn.forEach(VC -> System.out.print("Column " + VC.getColoana() + ": Value " + VC.getValoare() + "||\t"));
                            System.out.print("\n");
                        }
                );
    }
}
