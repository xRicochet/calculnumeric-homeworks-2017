import java.math.BigDecimal;

public class Tuple implements Comparable<Tuple> {

    private String polinom;
    private BigDecimal eroareaPolinomului;

    public Tuple(String polinom, BigDecimal eroareaPolinomului) {
        this.polinom = polinom;
        this.eroareaPolinomului = eroareaPolinomului;
    }

    public String getPolinom() {
        return polinom;
    }

    public void setPolinom(String polinom) {
        this.polinom = polinom;
    }

    public BigDecimal getEroareaPolinomului() {
        return eroareaPolinomului;
    }

    public void setEroareaPolinomului(BigDecimal eroareaPolinomului) {
        this.eroareaPolinomului = eroareaPolinomului;
    }


    @Override
    public int compareTo(Tuple o) {
        return this.eroareaPolinomului.compareTo(o.getEroareaPolinomului());
    }

    @Override
    public String toString() {
        return "\nTuple{" +
                "polinom='" + polinom + '\'' +
//                ", eroareaPolinomului=" + eroareaPolinomului +
                "}\n";
    }
}
