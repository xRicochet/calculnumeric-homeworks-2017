import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.regex.Matcher;

/**
 * Created by a.olteanu on 3/1/2017.
 */
public class Main {

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        double u = getMachinePrecision();

//        System.out.println("Precizia masina:\t" + u);

//        System.out.println("Este suma neasociativa?\t " + sumaAsociativiate(u));
//        System.out.println("Este produsul neasociativ?\t" + produsAsociativitate());

        aproximariPolinomialeAleFunctieiSin();

        long endTime = System.nanoTime();

        long duration = (endTime - startTime);

        System.out.println("\nExecution time:\t" + duration + " nanoseconds.");

    }

    private static double getMachinePrecision() {
        for (int i = 100; i > 1; i--) {
            double u = Math.pow(10, -i);
            if (1.0 + u != 1.0)
                return u;
        }
        return 0;
    }

    private static Boolean sumaAsociativiate(double u) {
        double x = 1.0;
        double y = u;
        double z = u;
        return (x + y) + z != x + (y + z);  //return true daca nu e asociativa
    }

    private static boolean produsAsociativitate() {
        Random random = new Random();
        double x, y, z;
        int i = 0;
        do {
            x = random.nextDouble();
            y = random.nextDouble();
            z = random.nextDouble();
            i++;
        } while (i < 1000000 && (((x * y) * z) == (x * (y * z))));

        System.out.println("x= " + x + "\ty= " + y + "\tz= " + z);
        System.out.println(((x * y) * z) + "\t-membrul stang\n" + (x * (y * z)) + "\t-membrul drept.");

        return (((x * y) * z) != (x * (y * z)));
    }

    private static void aproximariPolinomialeAleFunctieiSin() {

        String[][] matrix = new String[10000][3];
        double upperBound = Math.PI / 2;
        double lowerBound = -upperBound;
        System.out.println("LOWER BOUND: " + lowerBound);
        System.out.println("UPPER BOUND: " + upperBound);


        for (int i = 0; i < 2; i++) {
            double x = lowerBound + (upperBound - lowerBound) * new Random().nextDouble();
            List<Tuple> listaErori=new ArrayList<>();

            double sinX = Math.sin(x);

            listaErori.add(addError(x, sinX, 1));
            listaErori.add(addError(x, sinX, 2));
            listaErori.add(addError(x, sinX, 3));
            listaErori.add(addError(x, sinX, 4));
            listaErori.add(addError(x, sinX, 5));
            listaErori.add(addError(x, sinX, 6));
            Collections.sort(listaErori);
            System.out.println(listaErori);

            matrix[i][0] = listaErori.get(0).getPolinom();
            matrix[i][1] = listaErori.get(1).getPolinom();
            matrix[i][2] = listaErori.get(2).getPolinom();
        }
        afisareMatrice(matrix);
        //creat ierarhie
    }

    private static void afisareMatrice(String[][] matrix) {
        for(int i=0 ; i<10000; i++){
            System.out.println(matrix[i][0] + " | " +matrix[i][1] + " | " +matrix[i][2] + "|");
        }
    }

    private static Tuple addError(double x, double sinX, int functionIndex) {
        double p = functiaP(functionIndex, x);
        BigDecimal eroarePi = new BigDecimal(Math.abs(p - sinX));
        return new Tuple("p"+functionIndex,eroarePi);
    }

    private static double functiaP(int indexFunctie, double x) {
        double c1 = 1.0 / factorial(3), c2 = 1.0 / factorial(5);
        double c3 = 1.0 / factorial(7), c4 = 1.0 / factorial(9);
        double c5 = 1.0 / factorial(11), c6 = 1.0 / factorial(13);
        double x2 = Math.pow(x, 2);

        switch (indexFunctie) {
            case 1:
//                return x - c1 * Math.pow(x, 3) + c2 * Math.pow(x, 5);
                return x * (1 + x2 * (-c1 + c2 * x2));
            case 2:
//                return x - c1 * Math.pow(x, 3) + c2 * Math.pow(x, 5) - c3 * Math.pow(x, 7);
                return x * (1 + x2 * (-c1 + x2 * (c2 - c3 * x2)));
            case 3:
//                return x - c1 * Math.pow(x, 3) + c2 * Math.pow(x, 5) - c3 * Math.pow(x, 7) + c4 * Math.pow(x, 9);
                return x * (1 + x2 * (-c1 + x2 * (c2 + x2 * (-c3 + c4 * x2))));
            case 4:
//                return x - 0.166 * Math.pow(x, 3) + 0.00833 * Math.pow(x, 5) - c3 * Math.pow(x, 7) + c4 * Math.pow(x, 9);
                return x * (1 + x2 * (-0.166 + x2 * (0.00833 + x2 * (-c3 + c4 * x2))));
            case 5:
//                return x - c1 * Math.pow(x, 3) + c2 * Math.pow(x, 5) - c3 * Math.pow(x, 7) + c4 * Math.pow(x, 9) - c5 * Math.pow(x, 11);
                return x * (1 + x2 * (-c1 + x2 * (c2 + x2 * (-c3 + x2 * (c4 - c5 * x2)))));
            case 6:
//                return x - c1 * Math.pow(x, 3) + c2 * Math.pow(x, 5) - c3 * Math.pow(x, 7) + c4 * Math.pow(x, 9) - c5 * Math.pow(x, 11) + c6 * Math.pow(x, 13);
                return x * (1 + x2 * (-c1 + x2 * (c2 + x2 * (-c3 + x2 * (c4 + x2 * (-c5 + c6 * x2))))));
        }
        return 0;
    }

    private static double factorial(int index) {
        double factorial = 1.0;
        for (double i = 2.0; i <= index; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
